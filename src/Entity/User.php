<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\ApiResource;
use App\Controller\RegistrationController;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Attribute\Groups;
use ApiPlatform\Metadata\ApiProperty;
use App\Controller\ConfirmationEmailController;

#[ApiResource(
    operations: [
        new Post(
            name: 'registration',
            uriTemplate: '/registration',
            controller: RegistrationController::class,
            denormalizationContext: ['groups' => 'user:create'],
        ),
        new Get(
            name: 'confirmEmail',
            uriTemplate: '/confirmEmail/{verificationToken}',
            controller: ConfirmationEmailController::class
        ),
        new Get(
            name: 'getEmailResetPassword',
            uriTemplate: '/getEmailResetPassword/{email}',
            controller: 'App\Controller\ResetPasswordController::getEmailResetPassword',
        ),
        new Post(
            name: 'resetPassword',
            uriTemplate: '/resetPassword',
            controller: 'App\Controller\ResetPasswordController::resetPassword',
            denormalizationContext: ['groups' => 'user:reset']
        ),
        new Get(
            name : 'user',
            uriTemplate: '/user/{uuid_user}',
            security: "is_granted('ROLE_ADMIN') or is_granted('authorize', object)",
        )
        ],
    normalizationContext: ['groups' => ['user:read']]
)]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`users`')]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_EMAIL', fields: ['email'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[Groups(['user:create', 'user:read'])]
    #[Assert\NotBlank]
    #[Assert\Email]
    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[Groups(['user:create', 'user:reset'])]
    #[Assert\NotBlank]
    #[ORM\Column]
    private ?string $password = null;

    #[Groups(['user:create', 'user:read'])]
    #[Assert\NotBlank]
    #[ORM\Column(length: 80)]
    private ?string $firstname = null;

    #[Groups(['user:create', 'user:read'])]
    #[Assert\NotBlank]
    #[ORM\Column(length: 80)]
    private ?string $lastname = null;

    #[Groups(['user:read'])]
    #[ORM\Column(length: 60, unique: true)]
    private ?string $uuid_user = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $isVerified = false;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $verificationToken;

    #[Groups(['user:reset'])]
    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $resetToken;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }


    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }


    public function getUuidUser(): ?string
    {
        return $this->uuid_user;
    }

    public function setUuidUser(string $uuid_user): static
    {
        $this->uuid_user = $uuid_user;

        return $this;
    }
    public function getIsVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): static
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getVerificationToken(): ?string
    {
        return $this->verificationToken ?? 'done';
    }

    public function setVerificationToken(?string $resetToken): static
    {
        $this->verificationToken = $resetToken;

        return $this;
    }

    public function getResetToken(): ?string
    {
        return $this->resetToken;
    }

    public function setResetToken(?string $resetToken): static
    {
        $this->resetToken = $resetToken;

        return $this;
    }
}
