<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;


class MailerService
{

    public function __construct(
        private MailerInterface $mailer,
        private UrlGeneratorInterface $router,
        #[Autowire('%URL_WEB%')] private string $urlweb,
    ) {
    }

    public function sendMail($destinataire, $type = 'verification', $token = null, $headerSubject = null, $body = null)
    {

        $subject = match($type) {
            'verification' => 'Vérification adresse email SuperBowlBet',
            'reset' => 'Renouvellement mot de passe SuperBowlBet',
            default => $headerSubject
        };
    
        $message = match($type) {
            'verification' => 'Merci de vous êtes inscrit. Merci de cliquer sur le lien suivant pour confirmer votre adresse mail:',
            'reset' => 'Vous avez demandé une modifcation de votre mot de passe. Merci de  cliquer sur le lien suivant pour réinitialiser votre mot de passe:',
            default => $body
        };

        $link = match($type) {
            'verification' => 
                        $this->router->generate('confirmEmail', [
                        'verificationToken' => $token,
                        ], UrlGeneratorInterface::ABSOLUTE_URL),
            'reset' => $this->urlweb."forgetPassword/$token",   
            default => ''
        };

        $email = (new Email())
            ->from('noreply@sbb.com')
            ->to($destinataire)
            ->subject($subject)
            ->html(sprintf('<p>%s</p><p><a href="%s">%s</a></p>', $message, $link, $link));

        $this->mailer->send($email);
    }
}
