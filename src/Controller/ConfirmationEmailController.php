<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\RedirectResponse;

#[AsController]
class ConfirmationEmailController extends AbstractController
{

    public function __construct(private EntityManagerInterface $entityManager, private UserRepository $userRepository, #[Autowire('%URL_WEB%')] private string $urlweb)
    {
    }

    public function __invoke(string $verificationToken)
    {
    
        $user = $this->userRepository->findOneBy(['verificationToken' => $verificationToken]);

        if (!$user) {
            return new RedirectResponse($this->urlweb.'failedConfirm');
        }

        $user->setIsVerified(true);
        $user->setVerificationToken(null);

        $this->entityManager->flush();

        return new RedirectResponse($this->urlweb.'confirmSuccess');
    }
}
