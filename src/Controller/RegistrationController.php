<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class RegistrationController extends AbstractController
{

    public function __construct(private UserPasswordHasherInterface $passwordHasher, private MailerService $mailer, private EntityManagerInterface $em)
    {
    }

    public function __invoke(User $data)
    {

        $plainPassword = $data->getPassword();

        if (strlen($plainPassword) >= 8) {
            $hashedPassword = $this->passwordHasher->hashPassword($data, $plainPassword);
            $data->setPassword($hashedPassword);
        } else {
            throw new BadRequestException('Le mot de passe doit comporter au minimum 8 caractères.');
        }

        $data->setUuidUser(uniqid("U"));
        $data->setIsVerified(false);

        $token = md5(uniqid());
        $data->setVerificationToken($token);

        $this->mailer->sendMail($data->getEmail(), 'verification', $token);

        $this->em->persist($data);
        $this->em->flush();

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
}