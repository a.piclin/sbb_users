<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\MailerService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsController]
class ResetPasswordController extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $entityManager, 
        private UserRepository $userRepository,  
        private MailerService $mailer,
        private UserPasswordHasherInterface $passwordHasher
        )
    {
    }


    #[Route('/api/getEmailResetPassword/{email}', name: 'app_email_reset_password', methods: ['GET'])]
    public function getEmailResetPassword(string $email)
    {
    
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if (!$user) {
            return new JsonResponse(['Message' => 'Erreur'], 400);
        }

        $token = md5(uniqid());
        $user->setResetToken($token);

        $this->entityManager->flush();
        
        $this->mailer->sendMail($email, 'reset', $token);

        return new JsonResponse(['Message' => 'Email envoyé'], 200);
    }

    #[Route('/api/resetPassword', name: 'app_reset_password', methods: ['POST'])]
    public function resetPassword(Request $request)
    {
        $content = $request->getContent();

        $data = json_decode($content, true);

        $user = $this->userRepository->findOneBy(['resetToken' => $data['resetToken']]);

        if (!$user || $user->getIsVerified() !== true) {
            return new JsonResponse(['Message' => 'Erreur'], 400);
        }

        if (strlen($data['password']) < 8) {
            return new JsonResponse(['Message' => 'Le mot de passe doit être composé d\'au minimum 8 caractères'], 400);
        }

        $hashedPassword = $this->passwordHasher->hashPassword($user, $data['password']);
        $user->setPassword($hashedPassword);
        $user->setResetToken(null);

        $this->entityManager->flush();

        return new JsonResponse(['Message' => 'Modification du mot de passe réussie'], 200);

    }


}
